// Requiring express in the server
const express = require('express');
const app = express();
  
// Defining get request at '/' route
app.get('/', function(req, res) {
  res.json({
    number: 1
  });
});



  
// Defining get request at '/multiple' route
app.get('/json', function(req, res) {
  res.send(JSON.stringify({
    "mywidgets": [{
        "key": "name",
        "type": "Input",
        "label": "Name",
        "placeholder": "Enter Your Name",
        "required": true,
        "keyboardType": "text",
        "controller": "nameController",
        "decoration": {
          "type": "InputDecoration",
          "icon": "person",
          "fillColor": "grey",
          "filled": true,
          "hintText": "Enter your name",
          "labelText": "Name",
          "focusColor": "grey"
        },
        "validator": {
          "value": "value",
          "isEmpty": false,
          "length": 8
        }
      }, {
        "key": "email",
        "type": "Input",
        "label": "email",
        "required": true,
        "keyboardType": "text",
        "controller": "emailController",
        "decoration": {
          "type": "InputDecoration",
          "icon": "email",
          "fillColor": "grey",
          "filled": true,
          "hintText": "Enter your Email",
          "labelText": "Email ",
          "focusColor": "grey"
        },
        "validator": {
          "value": "value",
          "isEmpty": false,
          "length": 8,
          "isEmail": true
        }
      }, {
        "key": "number",
        "type": "Input",
        "label": "number",
        "required": true,
        "keyboardType": "number",
        "controller": "numberController",
        "decoration": {
          "type": "InputDecoration",
          "icon": "phone",
          "fillColor": "grey",
          "filled": true,
          "hintText": "Enter your number",
          "labelText": "Phone Nummber",
          "focusColor": "grey"
        },
        "validator": {
          "value": "value",
          "isEmpty": false,
          "length": 8
        }
      },
      {
        "key": "password",
        "type": "Input",
        "label": "Password",
        "required": true,
        "keyboardType": "text",
        "controller": "passwordController",
        "obscureText": true,
        "decoration": {
          "type": "InputDecoration",
          "icon": "password",
          "fillColor": "grey",
          "filled": true,
          "hintText": "Enter your password",
          "labelText": "password",
          "focusColor": "grey"
        },
        "validator": {
          "value": "value",
          "isEmpty": false,
          "length": 6
        }
      },
      {
        "key": "FemaleCB",
        "type": "DatePicker",
        "label": "Female"
      },
      {
        "key": "MaleCB",
        "type": "CheckBox",
        "label": "Male"
      },
      {
        "key": "FemaleCB",
        "type": "CheckBox",
        "label": "Female"
      }
    ]
  }
  ));
});
  

  
// Setting the server to listen at port 3000
app.listen(3000, function(req, res) {
  console.log("Server is running at port 3000");
});